﻿using UnityEngine;
using System.Collections;

public class ActivateFollow : MonoBehaviour
{
    bool activated = false;
    FollowPlayer follow;
    // Use this for initialization
    void Start()
    {
        follow = gameObject.GetComponent<FollowPlayer>();
    }

    // Update is called once per frame
    void Update()
    {

    }

    void OnTriggerExit(Collider coll)
    {
        if(coll.gameObject.tag == "Player")
        {
            follow.nearPlayer = false;
        }
    }

    void OnTriggerEnter(Collider coll)
    {
        if (coll.gameObject.tag == "Player")
        {
            if (activated == false)
            {
                follow.active = true;
            }
            follow.nearPlayer = true;
        }
    }
}


