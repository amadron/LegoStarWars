﻿using UnityEngine;
using System.Collections;

public class PlayerDetection : MonoBehaviour {
    public StormtrooperAI trooper;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	    
	}

    void OnTriggerEnter(Collider collider)
    {
        GameObject collObj = collider.gameObject;
        if(trooper != null && !trooper.inAlert && collObj.tag == "Player")
        {
            trooper.PlayerDetected(collObj);
        }
    }
}
