﻿using UnityEngine;
using System.Collections;

public class FollowPlayer : MonoBehaviour {
    public GameObject player;
    private Rigidbody rBody;
    public bool active = true;
    public bool nearPlayer = false;
    public float moveSpeed = 5.0f;
    public Animation animation;
    public string animationName;

	// Use this for initialization
	void Start () {
        rBody = gameObject.GetComponent<Rigidbody>();
	}

	// Update is called once per frame
	void Update () {
        //rBody.velocity = new Vector3(0, 0, 10);
        if (nearPlayer == false && active)
        {
            Vector3 pPos = player.transform.position;
            pPos.y = gameObject.transform.position.y;
            rBody.transform.LookAt(pPos);
            rBody.velocity = gameObject.transform.forward * moveSpeed;
            if(animation != null)
            {
                if(!animation.IsPlaying(animationName))
                {
                    animation.Play(animationName);
                }
            }
        }
        else
        {
            if (rBody != null && rBody.velocity != Physics.gravity)
            {
                Debug.Log("Second");
                rBody.velocity = Physics.gravity;
            }
        }
	}
}
