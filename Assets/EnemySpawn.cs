﻿using UnityEngine;
using System.Collections;

public class EnemySpawn : MonoBehaviour {
    public GameObject objectToSpawn;
    public Color gizmoColor = Color.cyan;
	// Use this for initialization
	void Start () {
	    if(objectToSpawn != null)
        {
            GameObject copy = Instantiate(objectToSpawn);
            copy.transform.position = gameObject.transform.position;
            copy.transform.rotation = gameObject.transform.rotation;
            copy.SetActive(true);
        }
	}
	
    void OnDrawGizmos()
    {
        Gizmos.color = Color.cyan;
        Gizmos.DrawSphere(transform.position, 0.5f);
    }
	// Update is called once per frame
	void Update () {
	
	}
}
