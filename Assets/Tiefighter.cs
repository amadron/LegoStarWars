﻿using UnityEngine;
using System.Collections;

public class Tiefighter : MonoBehaviour {
    private Rigidbody rBody;
	// Use this for initialization
	void Start () {
        rBody = gameObject.GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
	    if(Input.GetKeyDown(KeyCode.T))
        {
            ActivatePhysics();
        }
	}

    public void ActivatePhysics()
    {
        rBody.useGravity = true;
    }
}
