﻿using UnityEngine;
using System.Collections;

public class R2D2ActivateFollow : MonoBehaviour {
    FollowPlayer follow;
	// Use this for initialization
	void Start () {
        follow = gameObject.GetComponent<FollowPlayer>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    void OnTriggerEnter(Collider coll)
    {
        if (coll.gameObject.tag == "Player" && follow.nearPlayer != true)
        {
            follow.nearPlayer = true;
        }
    }

    void OnTriggerExit(Collider coll)
    {
        if (coll.gameObject.tag == "Player" && follow.nearPlayer != false)
        {
            follow.nearPlayer = false;
        }
    }
}
