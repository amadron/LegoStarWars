﻿using UnityEngine;
using System.Collections;
using System;
using UnityEngine.SceneManagement;

public class Intro : MonoBehaviour {
    public GameObject blackGround;
    public GameObject timeAgoText;
    public RectTransform logo;
    public GameObject text;

    public float logoMovementSpeed = 600.0f;
    public float textMovementSpeed = 20.0f;
    public float timeBeforeIntro = 3.0f;
    public float timeShowTimeAgo = 5.0f;
    public float pauseAfterTimeAgo = 3.0f;
    public float timeShowCrawl = 65.0f;
    public AudioSource music;
    int state = 0;
    float time;
    float targetTime;
	// Use this for initialization
	void Start () {
        targetTime = timeBeforeIntro;
        
	}
	
	// Update is called once per frame
	void Update () {
        time += Time.deltaTime;
        if(time >= targetTime)
        {
            switch(state)
            {
                //Show a long time ago
                case 0:
                    timeAgoText.SetActive(true);
                    targetTime += timeShowTimeAgo;
                    break;
                //Hide Long time Ago
                case 1:
                    timeAgoText.SetActive(false);
                    targetTime += pauseAfterTimeAgo;
                    break;
                //Start Crawl
                case 2:
                    blackGround.SetActive(false);
                    music.Play();
                    targetTime += timeShowCrawl;
                    Debug.Log("Show Crawl " + DateTime.Now);
                    break;
                case 3:
                    Debug.Log("Hide Crawl " + DateTime.Now);
                    break;
            }
            state++;
        }
        if(state == 3)
        {
            float moveLogo = logoMovementSpeed * Time.deltaTime;
            float moveText = textMovementSpeed * Time.deltaTime;
            logo.Translate(0, 0, moveLogo);
            text.transform.Translate(0, moveText, 0);
        }
        if(state == 4)
        {
            Finished();
        }
	}

    private void Finished()
    {
        SceneManager.LoadScene("Hangar");
    }
}
