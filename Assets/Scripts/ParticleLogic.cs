﻿using UnityEngine;
using System.Collections;

public class ParticleLogic : MonoBehaviour {
    private ParticleSystem particleSystem;
    public string tag;
	// Use this for initialization
	void Start () {
        particleSystem = gameObject.GetComponent<ParticleSystem>();
	}
	
	// Update is called once per frame
	void Update () {
	    if(!particleSystem.IsAlive())
        {
            Destroy(gameObject);
        }
	}
}
