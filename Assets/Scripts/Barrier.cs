﻿using UnityEngine;
using System.Collections;

public class Barrier : MonoBehaviour {

    public GameLogic gameLogic;
	// Use this for initialization
	void Start () {
	
	}

    void OnTriggerEnter(Collider collider)
    {
        if(collider.gameObject.tag == "Enemy")
        {
            gameLogic.GameOver();
        }
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
