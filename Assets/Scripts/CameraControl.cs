﻿using UnityEngine;
using System.Collections;

public class CameraControl : MonoBehaviour {
    public Camera playerCamera;
    public BoxCollider boxCollider;
    public GameObject playerModel;
    public float moveSpeed = 5.0f;
    public bool outOfRange = false;
	// Use this for initialization
	void Start () {
        boxCollider = gameObject.GetComponent<BoxCollider>();
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.C))
            RotateCamera();
        if (outOfRange)
        {
            Vector3 rangeVector = getNextToRange();
            Vector3 correct = Vector3.zero;
            if(rangeVector.x != 0)
            {
                correct.x = rangeVector.x * moveSpeed * Time.deltaTime;
            }
            if(rangeVector.z != 0)
            {
                //Debug.Log("Dir camera:" + playerCamera.transform.forward + " rangeVector:" + rangeVector);
                if ((playerCamera.transform.forward.z * rangeVector.z) < 0)
                {
                    RotateCamera();
                    Debug.Log("Rotate!");
                }
                correct.z = rangeVector.z * moveSpeed * Time.deltaTime;

            }
            gameObject.transform.Translate(correct);
        }
	}

    void OnTriggerExit(Collider collider)
    {
        if(collider.gameObject.tag == "Player")
            outOfRange = true;
    }

    void OnTriggerEnter(Collider collider)
    {
        if(collider.gameObject.tag == "Player")
            outOfRange = false;
    }

    Vector3 getNextToRange()
    {
        Bounds collBounds = boxCollider.bounds;
        Vector3 boxSize = collBounds.size;
        Vector3 pos = gameObject.transform.position;
        float left = pos.x - boxSize.x / 2;
        float right = pos.x + boxSize.x / 2;
        float forward = pos.z + boxSize.z / 2;
        float back = pos.z - boxSize.z / 2;
        Vector3 playerPos = playerModel.transform.position;

        Vector3 camDir = playerCamera.transform.forward;
        Vector3 move = Vector3.zero;
        Vector3 collDir = playerModel.transform.forward;
        if (playerPos.x > right)
        {
            move.x = 1;
        }
        if (playerPos.x < left)
        {
            move.x = -1;
        }
        if (playerPos.z > forward)
        {
            move.z = 1;
        }
        if (playerPos.z < back)
        {
            move.z = -1;
        }
        return move;
    }

    private void RotateCamera()
    {
        playerCamera.transform.RotateAround(gameObject.transform.position, Vector3.up, 180);
    }
}
