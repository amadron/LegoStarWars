﻿using UnityEngine;
using System.Collections;

public class Shotlogic : MonoBehaviour {
    public string tag;
    public float lifetime = 5.0f;
    private float time = 0;
    public GameObject collisionParticle;
	// Use this for initialization
	void Start () {
        
	}
	
    void OnTriggerEnter(Collider collider)
    {
        hit(collider.gameObject);
        /*
        GameObject coll = Instantiate(collisionParticle);
        coll.transform.position = collider.transform.position;
        coll.transform.rotation = Quaternion.LookRotation(gameObject.transform.up);
        coll.SetActive(true);
        Destroy(gameObject);
        */
    }

	// Update is called once per frame
	void Update () {
        time += Time.deltaTime;
        if(time >= lifetime)
        {
            Debug.Log("Destroyed Lasershot after: " + time);
            Destroy(gameObject);
        }
	}

    void OnCollisionEnter(Collision collision)
    {
        hit(collision.gameObject);
        /*GameObject coll = Instantiate(collisionParticle);
        foreach (ContactPoint point in collision.contacts)
        {
            coll.transform.position = point.point;
            coll.transform.rotation = Quaternion.LookRotation(point.normal);
            break;
        }
        coll.SetActive(true);
        Destroy(gameObject);*/
    }

    private void hit(GameObject obj)
    {
        Physicsstate state = obj.GetComponent<Physicsstate>();
        if (state != null)
        {
            if (tag == "Player" && state.stormTrooper != null)
            {
                state.stormTrooper.decrementHealth();
            }
            if (tag == "Enemy" && state.player != null)
            {
                state.player.decreaseHealth();
            }
        }
    }

}
