﻿using UnityEngine;
using System.Collections;

public class Physicsstate : MonoBehaviour {
    public bool onGround;
    public Player player;
    public StormtrooperAI stormTrooper;
    // Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnCollisionStay(Collision collision)
    {
        if(onGround == false)
        {
            onGround = true;
        }
    }

    void OnCollisionExit(Collision collision)
    {
        onGround = false;
    }
}
