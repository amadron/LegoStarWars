﻿using UnityEngine;
using System.Collections;

public class Control : MonoBehaviour {

    public GameObject laserShot;
    private Rigidbody rBody;
    public float movSpeed = 1.0f;
    public float shotSpeed = 5.0f;
    public GameObject laserEmitter;
    public Animation anim;
    public GameLogic gameLogic;
    public int health = 3;
	// Use this for initialization
	void Start () {
        rBody = gameObject.GetComponent<Rigidbody>();
        Debug.Log("LaserEmitter:" + laserEmitter.name);
        gameLogic.SetHealthValueText(health);
	}
	
	// Update is called once per frame
	void Update () {
        Vector3 pos = rBody.position;
	    if(Input.GetKey(KeyCode.A))
        {
            pos.x -= movSpeed;
            if(!anim.isPlaying)
                anim.Play("Basis");
        }
        if(Input.GetKey(KeyCode.D))
        {
            pos.x += movSpeed;
            if(!anim.isPlaying)
                anim.Play("Basis");
        }
        if(Input.GetKeyDown(KeyCode.F))
        {
            Shoot();
        }
        if(rBody != null && pos != rBody.position)
        {
            rBody.MovePosition(pos);
        }
	}

    public void DecreaseHealth()
    {
        health--;
        gameLogic.SetHealthValueText(health);
        if(health <= 0)
        {
            gameLogic.GameOver();
        }
    }

    private void Shoot()
    {
        if (laserEmitter != null)
        {
            GameObject shot = Instantiate(laserShot);
            Vector3 pos = laserEmitter.transform.position;
            Bounds shotBounds = shot.GetComponent<MeshRenderer>().bounds;
            pos.z += shotBounds.size.z/2 + 0.1f;
            Rigidbody shotBody = shot.GetComponent<Rigidbody>();
            shot.transform.position = pos;
            Vector3 velo = laserEmitter.transform.forward;
            velo.z = shotSpeed;
            shotBody.velocity = velo;
            shot.SetActive(true);
        }
    }
}
