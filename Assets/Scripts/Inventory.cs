﻿using UnityEngine;
using System.Collections;

public class Inventory : MonoBehaviour {
    public GameObject weapon;
    public GameObject weaponSlot;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void setWeapon(GameObject weapon)
    {
        if (weapon.GetComponent<Weapon>() != null)
        {
            weapon.transform.position = weaponSlot.transform.position;
            weapon.transform.rotation = Quaternion.LookRotation(weaponSlot.transform.forward);
            //weapon.transform.Rotate(0, 180, 0);
            weapon.transform.SetParent(weaponSlot.transform);
            this.weapon = weapon;
        }
    }

}
