﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Player : MonoBehaviour {
    private Rigidbody rBody;
    public GameObject model;
    public Camera playerCam;
    private Inventory inventory;
    public GameObject gameOverText;

    public Animation anim;

    public int health = 3;
    public int score = 0;

    public float movementSpeed = 20.0f;
    public float jumpTime = 0.02f;
    private float jumpCountdown;
    bool jump = false;
    public float jumpforce = 10.0f;
    Physicsstate physState;

    public Text scoreDisplay;
    public Text healthDisplay;
    // Use this for initialization
    void Start()
    {
        if (model != null)
        {
            rBody = model.GetComponent<Rigidbody>();
            physState = model.GetComponent<Physicsstate>();
        }
        healthDisplay.text = health.ToString();
        inventory = gameObject.GetComponent<Inventory>();
    }
	
	// Update is called once per frame
	void Update () {
        Vector3 dir = Vector3.zero;
        bool walkAnim = false;
        if(Input.GetKey(KeyCode.F))
        {
            if(inventory.weapon != null)
            {
                if (inventory.weapon.GetComponent<Weapon>().Fire(gameObject.tag))
                    anim.Play("Shoot");
            }
        }
	    if(Input.GetKey(KeyCode.A))
        {
            dir -= playerCam.transform.right.normalized;
            walkAnim = true;
        }
        if(Input.GetKey(KeyCode.D))
        {
            dir += playerCam.transform.right.normalized;
            walkAnim = true;
        }
        if(Input.GetKey(KeyCode.W))
        {
            dir += playerCam.transform.forward.normalized;
            walkAnim = true;
        }
        if(Input.GetKey(KeyCode.S))
        {
            dir -= playerCam.transform.forward.normalized;
            walkAnim = true;
        }
        if(walkAnim && !anim.isPlaying)
        {
            anim.Play("Walking");
        }
        if(!walkAnim && anim.IsPlaying("Walking"))
        {
            anim.Stop();
        }
        if (dir.z > 0)
            dir.z = 1;
        if (dir.z < 0)
            dir.z = -1;
        dir.y = 0;
        if(Input.GetKey(KeyCode.Space))
        {
            if(!jump && physState.onGround)
            { 
                jumpCountdown = jumpTime;
                jump = true;
            }
        }
        if (jump)
        {
            jumpCountdown -= Time.deltaTime;
            if (jumpCountdown <= 0)
                jump = false;
        }
        if (dir != Vector3.zero)
        {
            Quaternion dirRot = Quaternion.LookRotation(dir);
            if (rBody.rotation != dirRot)
                rBody.rotation = dirRot;
        }
        dir *= movementSpeed;
        if (!jump)
        {
            dir += Physics.gravity;
        }
        else
        {
            dir.y = jumpforce;
        }
        rBody.velocity = dir;
        //Debug.Log("rBody.velocity: " + rBody.velocity);
	}

    public void incrementScore(int amount)
    {
        score += amount;
        if(scoreDisplay != null)
            scoreDisplay.text = score.ToString();
    }

    public void decreaseHealth()
    {
        health--;
        if(healthDisplay != null)
            healthDisplay.text = health.ToString();
        if (health <= 0)
            gameOverText.SetActive(true);
            Time.timeScale = 0;
    }

    
}
