﻿using UnityEngine;
using System.Collections;

public abstract class Entity : MonoBehaviour {
    public int health = 1;

	// Use this for initialization
	void Start () {
	
	}

    abstract public void decrementHealth();
	// Update is called once per frame
	void Update () {
	
	}
}
