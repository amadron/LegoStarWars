﻿using UnityEngine;
using System.Collections;

public abstract class Weapon : MonoBehaviour {

    public GameObject projectileEmitter;
    public GameObject projectile;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public abstract bool Fire(string tag);
}
