﻿using UnityEngine;
using System.Collections;
using System;

public class Blaster : Weapon {

    public float shootVelocity = 5.0f;
    public float rotationPerSecond = 3.0f;
    public int status = 0;
    public float shotCooldown = 1.0f;
    private float cooldown = 0;
    bool willShoot = false;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
        if(status == 0)
        {
            gameObject.transform.Rotate(0, rotationPerSecond * Time.deltaTime, 0);
        }
        if(cooldown > 0)
        {
            cooldown -= Time.deltaTime;
        }
	}

    public override bool Fire(String tag)
    {
        if (projectile != null && projectileEmitter != null && cooldown <= 0)
        {
            GameObject shot = Instantiate(projectile);
            shot.GetComponent<Shotlogic>().tag = tag;
            Bounds shotBounds = shot.GetComponent<MeshFilter>().mesh.bounds;
            Debug.Log("shotBounds:" + shotBounds);
            Vector3 velo = projectileEmitter.transform.forward;
            Vector3 pos = projectileEmitter.transform.position;
            Vector3 dir = projectileEmitter.transform.rotation.eulerAngles;
            dir.x = 90;
            shot.transform.rotation = Quaternion.Euler(dir);
            //pos.z += (shotBounds.size.y / 2) + 5;
            Rigidbody shotBody = shot.GetComponent<Rigidbody>();
            shot.transform.position = pos;
            shot.SetActive(true);
            velo.y = 0;
            shotBody.velocity = velo * shootVelocity;
            cooldown = shotCooldown;
            return true;
        }
        else
        {
            return false;
        }
    }

    void OnTriggerEnter(Collider collider)
    {
        GameObject collObj = collider.gameObject;
        if (collObj.tag == "Player")
        {
            Physicsstate physState = collObj.GetComponent<Physicsstate>();
            if (physState != null && physState.player != null)
                physState.player.GetComponent<Inventory>().setWeapon(gameObject);
            gameObject.GetComponent<BoxCollider>().isTrigger = false;
            status++;
        }
    }
}
