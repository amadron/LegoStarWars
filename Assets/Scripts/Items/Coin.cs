﻿using UnityEngine;
using System.Collections;

public class Coin : MonoBehaviour {

    public int scoreAmount = 1;
    public Animation animation;
	// Use this for initialization
	void Start () {
	    
	}
	
	// Update is called once per frame
	void Update () {
	    if(animation != null && !animation.isPlaying)
        {
            animation.Play();
        }
	}

    void OnTriggerEnter(Collider collider)
    {
        Debug.Log("Collided with coin!");

        Player player = null;
        Physicsstate phys = collider.gameObject.GetComponent<Physicsstate>();
        if (phys != null)
            player = phys.player;
        if(player != null)
        {
            player.incrementScore(scoreAmount);
            Destroy(gameObject);
        }
    }
}
