﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Menulogic : MonoBehaviour {
    public float blinkspeed = 5.0f;
    public AudioSource menuMusic;
    public Camera cam;
    public Player playerScript;
    public GameObject camTargetPos;
    public float camMoveSpeed = 1.0f;
    public GameObject gameUI;

    public Text startText;
    public Image enterBackground;
    public RawImage enterLogo;
    int decDir = -1;
    int state = 0;
    // Use this for initialization
    float time;
	void Start () {
        if (menuMusic != null && menuMusic.clip != null)
            menuMusic.Play();
	}
    Vector3 toMove;
	// Update is called once per frame
	void Update () {
        if(state == 0)
            changeAlpha();
        if(Input.GetKeyDown(KeyCode.Return))
        {
            Vector3 currPos = cam.transform.position;
            Vector3 targetPos = camTargetPos.transform.position;
            Vector3 diff = targetPos - currPos;
            
            float toX = diff.x/(float)(camMoveSpeed);
            if (float.IsInfinity(toX))
                toX = 0;
            float toY = diff.y/(float)(camMoveSpeed);
            if (float.IsInfinity(toY))
                toY = 0;
            float toZ = diff.z/(float)(camMoveSpeed);
            if (float.IsInfinity(toZ))
                toZ = 0;
            toMove = new Vector3(toX, toY, toZ);
            
            //toMove = diff;
            time = 0;
            state++;
        }
        if(state == 1)
        {
            if(time <= camMoveSpeed)
            {
                camMove();
            }
            else
            {
                StartGame();
            }
        }
	}

    private void StartGame()
    {
        if (menuMusic != null && menuMusic.clip != null)
            menuMusic.volume *= 0.2f;
        gameObject.SetActive(false);
        if (gameUI != null)
            gameUI.SetActive(true);
        if (playerScript != null)
            playerScript.enabled = true;
    }

    private void camMove()
    {
        time += Time.deltaTime;
        if (time <= camMoveSpeed)
        {
            Vector3 trans = new Vector3(toMove.x * Time.deltaTime, toMove.y * Time.deltaTime, toMove.z * Time.deltaTime);
            cam.transform.Translate(trans);
        }
    }

    private void changeAlpha()
    {
        float dec = blinkspeed * decDir * Time.deltaTime;
        Color textColor = startText.color;
        textColor.a += dec;
        startText.color = textColor;
        Color backColor = enterBackground.color;
        backColor.a += dec;
        enterBackground.color = backColor;
        Color enterColor = enterLogo.color;
        enterColor.a += dec;
        if (textColor.a <= 0)
        {
            decDir = 1;
            //Debug.Log("Increment now!");
        }
        if (textColor.a >= 1)
        {
            decDir = -1;
            //Debug.Log("Decrement now!");
        }
        enterLogo.color = enterColor;
    }
}
