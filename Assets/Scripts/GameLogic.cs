﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameLogic : MonoBehaviour {

    public Text healthValueText;

	// Use this for initialization
	void Start () {
	
	}
	
    public void SetHealthValueText(int value)
    {
        healthValueText.text = value.ToString();
    }

    public void GameOver()
    {
        Debug.Log("Game Over!");
        Time.timeScale = 0;
    }

    public void GameWin()
    {
        Debug.Log("You won!");
    }

	// Update is called once per frame
	void Update () {
	
	}
}
