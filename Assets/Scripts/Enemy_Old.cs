﻿using UnityEngine;
using System.Collections;

public class Enemy : MonoBehaviour {

    public int health = 1;
    public float forwardSpeed = 0.1f;
    public float sidewardSpeed = 0.3f;
    public float shootIntervall = 5.0f;
    public float shotSpeed = 5.0f;
    private Rigidbody rBody;
    public GameObject laserShot;
    public GameObject laserEmitter;
    public GameLogic gameLogic;
    float shotCountdown;

	// Use this for initialization
	void Start () {
        rBody = gameObject.GetComponent<Rigidbody>();
        Shoot();
        shotCountdown = shootIntervall;
	}
	
    public void DecreaseHealth()
    {
        health--;
        if(health <= 0)
        {
            Debug.Log("Enemy dies");
            Die();
        }
    }

    private void Die()
    {
        Destroy(gameObject);
    }

    void OnCollisionEnter(Collision collision)
    {
        sidewardSpeed *= -1;
    }
	// Update is called once per frame
	void Update () {
        shotCountdown -= Time.deltaTime;
        if(shotCountdown <= 0)
        {
            Shoot();
            shotCountdown = shootIntervall;
        }
        Vector3 forward = rBody.gameObject.transform.forward;
        forward *= forwardSpeed;
        forward.x = sidewardSpeed;
        Vector3 currPos = rBody.position;
        rBody.MovePosition(forward + currPos);
	}

    private void Shoot()
    {
        if (laserEmitter != null)
        {
            GameObject shot = Instantiate(laserShot);
            Vector3 pos = laserEmitter.transform.position;
            Debug.Log("EnemyLaser Pos:" + pos + " Enemy Pos: " + gameObject.transform.position);
            Bounds shotBounds = shot.GetComponent<MeshRenderer>().bounds;
            pos.z -= shotBounds.size.z / 2 + 0.1f;
            Rigidbody shotBody = shot.GetComponent<Rigidbody>();
            shot.transform.position = pos;
            Vector3 velo = laserEmitter.transform.forward * -1;
            velo.z *= shotSpeed;
            shotBody.velocity = velo;
            shot.SetActive(true);
        }
    }
}
