﻿using UnityEngine;
using System.Collections;

public class StormtrooperAI : MonoBehaviour {
    public GameObject player;
    public GameObject trooperModel;
    private Inventory inventory;
    public bool inAlert;
    private Rigidbody rBody;
    public int health = 1;

    int state;
	// Use this for initialization
	void Start () {
        state = 0;
        inAlert = false;
        inventory = gameObject.GetComponent<Inventory>();
        if (trooperModel != null)
            rBody = trooperModel.GetComponent<Rigidbody>();
	}
	
	// Update is called once per frame
	void Update () {
	    if(state == 2 && player != null)
        {
            Vector3 playerPos = player.transform.position;
            playerPos.y = rBody.transform.position.y;
            rBody.transform.LookAt(playerPos);
            if(inventory.weapon != null)
            {
                inventory.weapon.GetComponent<Weapon>().Fire(gameObject.tag);
            }

        }
	}

    public void decrementHealth()
    {
        health--;
        if(health < 0)
        {
            Destroy(gameObject);
        }
    }

    public void PlayerDetected(GameObject player)
    {
        this.player = player;
        state = 2;
        Debug.Log("Player detected!");
    }
}
